const std = @import("std");
const print = std.debug.print;
const assert = std.debug.assert;
const Allocator = std.mem.Allocator;

const sdl = @import("c.zig");
const Player = @import("Player.zig");
const World = @import("World.zig");
const Platform = @import("Platform.zig");

const Pixel = u32;

pub fn main() !void {
    const width = 800;
    const height = 600;

    const WHITE = 0xFFFFFFFF;
    const BLACK = 0xFF000000;
    const RED = 0xFFFF4444;
    const GREEN = 0xFF44FF44;
    const BLUE = 0xFF4444FF;

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    defer _ = gpa.deinit();

    const init_status: i32 = sdl.SDL_Init(sdl.SDL_INIT_VIDEO);
    if (init_status != 0) {
        std.os.exit(1);
    }

    const window: *sdl.SDL_Window = sdl.SDL_CreateWindow("sdl app", 0, 0, width, height, sdl.SDL_WINDOW_SHOWN | sdl.SDL_WINDOW_RESIZABLE).?;

    const renderer: *sdl.SDL_Renderer = sdl.SDL_CreateRenderer(window, -1, sdl.SDL_RENDERER_ACCELERATED).?;
    _ = sdl.SDL_SetRenderDrawBlendMode(renderer, sdl.SDL_BLENDMODE_BLEND);

    const bg: *sdl.SDL_Texture = sdl.SDL_CreateTexture(renderer, sdl.SDL_PIXELFORMAT_ARGB8888, sdl.SDL_TEXTUREACCESS_STREAMING, width, height).?;
    _ = sdl.SDL_SetTextureBlendMode(bg, sdl.SDL_BLENDMODE_BLEND); // <- enable transparency (2/2)

    // make bg into memory
    var canvas: [width * height]Pixel = undefined;
    for (canvas, 0..) |_, i| {
        if (i < width * height / 2) {
            canvas[i] = WHITE;
        } else {
            canvas[i] = RED;
        }
    }

    // ~~~~ INIT GAME STRUCTS ~~~~

    var p1 = try Platform.init(allocator, 100, 300, 500, 20, BLUE);
    defer p1.deinit();

    var p2 = try Platform.init(allocator, 350, 250, 300, 50, BLACK);
    defer p2.deinit();

    var platforms = [_]Platform{ p1, p2 };
    var world = World{ .platforms = platforms[0..], .floor_height = height - height / 4 };
    var player = try Player.init(allocator, 20, world.floor_height, 50, 50, GREEN);
    defer player.deinit();

    var frame_start: u32 = 0;
    var frame_end: u32 = 0;
    var frame_count: u32 = 0;

    var quit: bool = false;

    var g: f16 = 1;

    while (!quit) {
        frame_count = sdl.SDL_GetTicks();
        frame_start = frame_count;

        var event: sdl.SDL_Event = undefined;
        while (sdl.SDL_PollEvent(&event) != 0) {
            switch (event.type) {
                sdl.SDL_QUIT => {
                    quit = true;
                },

                sdl.SDL_KEYDOWN => {
                    switch (event.key.keysym.sym) {
                        sdl.SDLK_ESCAPE => {
                            quit = true;
                        },

                        sdl.SDLK_SPACE => {
                            if (player.can_jump) {
                                player.jump();
                            }
                        },

                        sdl.SDLK_q => {
                            quit = true;
                        },

                        sdl.SDLK_h => {
                            player.xvel = -player.speed;
                        },

                        sdl.SDLK_l => {
                            player.xvel = player.speed;
                        },

                        sdl.SDLK_LEFT => {
                            world.scroll(-2);
                        },

                        sdl.SDLK_RIGHT => {
                            world.scroll(2);
                        },

                        sdl.SDLK_UP => {},

                        sdl.SDLK_DOWN => {},

                        else => {},
                    }
                },

                sdl.SDL_KEYUP => {
                    switch (event.key.keysym.sym) {
                        sdl.SDLK_SPACE => {},

                        sdl.SDLK_h => {
                            if (player.xvel < 0) {
                                player.xvel = 0;
                            }
                        },

                        sdl.SDLK_l => {
                            if (player.xvel > 0) {
                                player.xvel = 0;
                            }
                        },

                        sdl.SDLK_LEFT => {},

                        sdl.SDLK_RIGHT => {},

                        sdl.SDLK_UP => {},

                        sdl.SDLK_DOWN => {},

                        else => {},
                    }
                },

                sdl.SDL_MOUSEWHEEL => {
                    if (event.wheel.y > 0) {} else if (event.wheel.y < 0) {}
                },

                sdl.SDL_MOUSEBUTTONDOWN => {
                    if (event.button.button == sdl.SDL_BUTTON_LEFT) {} else if (event.button.button == sdl.SDL_BUTTON_RIGHT) {}
                },

                sdl.SDL_MOUSEBUTTONUP => {
                    if (event.button.button == sdl.SDL_BUTTON_LEFT) {} else if (event.button.button == sdl.SDL_BUTTON_RIGHT) {}
                },

                sdl.SDL_MOUSEMOTION => {},
                else => {},
            }
        }

        _ = sdl.SDL_UpdateTexture(bg, null, @ptrCast(canvas[0..]), width * @sizeOf(Pixel));

        _ = sdl.SDL_RenderClear(renderer);

        // NOTE : order matters : draw bg, then draw platforms, then draw player !!
        _ = sdl.SDL_RenderCopy(renderer, bg, null, null);

        inline for (platforms) |platform| {
            platform.draw(renderer);
        }

        player.draw(renderer);

        _ = sdl.SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // < set drawing color to white after drawing player

        _ = sdl.SDL_RenderPresent(renderer);

        world.handleMovement(&player);

        // ~~~~ regulate fps ~~~~
        frame_end = sdl.SDL_GetTicks();
        const elapsed_time = frame_end - frame_start;
        const time_wanted = 1000 / 60;

        if (elapsed_time < time_wanted) {
            sdl.SDL_Delay(time_wanted - elapsed_time);
        }

        // allow player to jump again when he gets on floor
        if (player.sdlrect.y + player.sdlrect.h == world.floor_height) {
            player.can_jump = true;
            g = 1;
        }

        // allow player to jump again when steps on platform
        if (player.jump_steps == 0 and world.isOnAnyPlatform(player)) {
            player.can_jump = true;
            g = 1;
        }

        // disable jump when falling
        if (!world.isOnAnyPlatform(player) and player.sdlrect.y + player.sdlrect.h < world.floor_height) {
            player.can_jump = false;
        }

        // unsure player doesn't get under ground
        while (player.sdlrect.y + player.sdlrect.h > world.floor_height) player.sdlrect.y -= 1;

        // apply gravity on player
        if (player.jump_steps == 0 and !world.isOnAnyPlatform(player) and player.sdlrect.y + player.sdlrect.h < world.floor_height) {
            const fall_dist: i32 = @intFromFloat(g);

            var y_save = player.sdlrect.y;
            player.sdlrect.y += fall_dist;

            if (player.sdlrect.y + player.sdlrect.h > world.floor_height or world.isInsideAnyPlatform(player)) {
                // here, player is not on platform or gnd, but fall_dist is too big. Apply baby steps increase.
                player.sdlrect.y = y_save;

                var k: i32 = 0;

                while (k < fall_dist) : (k += 1) {
                    y_save = player.sdlrect.y;
                    player.sdlrect.y += 1;

                    if (player.sdlrect.y + player.sdlrect.h > world.floor_height or world.isInsideAnyPlatform(player)) {
                        player.sdlrect.y = y_save;
                        break;
                    }
                }
            }

            g += 0.5;
        }
    }

    sdl.SDL_DestroyTexture(bg);
    sdl.SDL_DestroyRenderer(renderer);
    sdl.SDL_DestroyWindow(window);
    sdl.SDL_Quit();
}
