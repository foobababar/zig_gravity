const std = @import("std");
const print = std.debug.print;

const Platform = @import("Platform.zig");
const Player = @import("Player.zig");

const Self = @This();

platforms: []Platform,
floor_height: i32,

fn isOn(self: Self, player: Player, platform: Platform) bool {
    _ = self;
    const x = player.sdlrect.x;
    const y = player.sdlrect.y;
    const w = player.sdlrect.w;
    const h = player.sdlrect.h;

    const xp = platform.sdlrect.x;
    const yp = platform.sdlrect.y;
    const wp = platform.sdlrect.w;

    if ((x + w > xp and x + w < xp + wp) and
        (y + h == yp))
    {
        return true;
    }

    if ((x > xp and x < xp + wp) and
        (y + h == yp))
    {
        return true;
    }

    return false;
}

pub fn isOnAnyPlatform(self: Self, player: Player) bool {
    for (self.platforms) |p| {
        if (isOn(self, player, p)) {
            return true;
        }
    }
    return false;
}

fn pointInsidePlatform(self: Self, x: i32, y: i32, p: Platform) bool {
    _ = self;
    if ((x >= p.sdlrect.x and x < p.sdlrect.x + p.sdlrect.w) and
        (y >= p.sdlrect.y and y < p.sdlrect.y + p.sdlrect.h))
    {
        return true;
    } else {
        return false;
    }
}

fn isInside(self: Self, player: Player, p: Platform) bool {
    // FIXME : only check for outer layer of pixels
    var j = player.sdlrect.y;
    while (j < player.sdlrect.y + player.sdlrect.h) : (j += 1) {
        var i = player.sdlrect.x;
        while (i < player.sdlrect.x + player.sdlrect.w) : (i += 1) {
            if (pointInsidePlatform(self, i, j, p)) return true;
        }
    }
    return false;
}

pub fn isInsideAnyPlatform(self: Self, p: Player) bool {
    for (self.platforms) |platform| {
        if (isInside(self, p, platform)) return true;
    }
    return false;
}

fn handleXMovement(self: Self, player: *Player) void {

    // baby steps increase
    var x_save = player.sdlrect.x;
    player.sdlrect.x += player.xvel;

    if (isInsideAnyPlatform(self, player.*)) {
        player.sdlrect.x = x_save;

        var i: i32 = if (player.xvel >= 0) player.xvel else -player.xvel;

        while (i > 0) : (i -= 1) {
            x_save = player.*.sdlrect.x;
            player.sdlrect.x += @divExact(player.xvel, 5);
            if (isInsideAnyPlatform(self, player.*)) {
                player.sdlrect.x = x_save;
                break;
            }
        }
    }
}

fn handleYMovement(self: Self, player: *Player) void {
    if (player.jump_steps > 0) {
        player.jump_steps -= 1;

        var y_save = player.sdlrect.y;
        player.sdlrect.y -= player.yvel;

        if (isInsideAnyPlatform(self, player.*)) {
            player.sdlrect.y = y_save;

            // baby steps increase
            var k: i32 = 0;

            while (k < player.yvel) : (k += 1) {
                y_save = player.sdlrect.y;
                player.sdlrect.y -= 1;
                if (isInsideAnyPlatform(self, player.*)) {
                    player.sdlrect.y = y_save;
                }
            }
        }
    }
}

pub fn handleMovement(self: Self, player: *Player) void {
    handleXMovement(self, player);
    handleYMovement(self, player);
}

pub fn scroll(self: Self, x: i32) void {
    for (self.platforms) |platform| {
        platform.sdlrect.x += x;
    }
}
