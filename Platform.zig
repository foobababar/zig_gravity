const std = @import("std");
const print = std.debug.print;
const assert = std.debug.assert;
const Allocator = std.mem.Allocator;
const Pixel = u32;
const sdl = @import("c.zig");
const Platform = @import("Platform.zig");

const Self = @This();
allocator: Allocator,
color: Pixel,
sdlrect: *sdl.SDL_Rect,

pub fn init(allocator: Allocator, x: i32, y: i32, w: i32, h: i32, color: Pixel) !Self {
    var tmp = try allocator.create(sdl.SDL_Rect);
    tmp.x = x;
    tmp.y = y;
    tmp.w = w;
    tmp.h = h;

    return Self{
        .allocator = allocator,
        .color = color,
        .sdlrect = tmp,
    };
}

pub fn deinit(self: Self) void {
    self.allocator.destroy(self.sdlrect);
}

fn pixel2rgb(pixel: Pixel) struct { r: u8, g: u8, b: u8 } {
    return .{
        .r = @intCast((pixel >> 8 * 2) & 0xFF),
        .g = @intCast((pixel >> 8 * 1) & 0xFF),
        .b = @intCast((pixel >> 8 * 0) & 0xFF),
    };
}

pub fn draw(self: Self, renderer: *sdl.SDL_Renderer) void {
    const rgb = pixel2rgb(self.color);
    _ = sdl.SDL_SetRenderDrawColor(renderer, rgb.r, rgb.g, rgb.b, 255);
    _ = sdl.SDL_RenderFillRect(renderer, @ptrCast(self.sdlrect));
}
