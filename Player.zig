const std = @import("std");
const print = std.debug.print;
const Allocator = std.mem.Allocator;

const sdl = @import("c.zig");

const Pixel = u32;
const Self = @This();
allocator: Allocator,
sdlrect: *sdl.SDL_Rect,
color: Pixel,
xvel: i32,
yvel: i32,
can_jump: bool,
jump_steps: i8,
speed: i32,

pub fn init(allocator: Allocator, x: i32, y: i32, w: i32, h: i32, color: Pixel) !Self {
    var tmp = try allocator.create(sdl.SDL_Rect);
    tmp.x = x;
    tmp.y = y;
    tmp.w = w;
    tmp.h = h;

    return Self{
        .allocator = allocator,
        .sdlrect = tmp,
        .color = color,
        .xvel = 0,
        .yvel = 50,
        .can_jump = true,
        .jump_steps = 0,
        .speed = 5,
    };
}

pub fn deinit(self: Self) void {
    self.allocator.destroy(self.sdlrect);
}

fn pixel2rgb(pixel: Pixel) struct { r: u8, g: u8, b: u8 } {
    return .{
        .r = @intCast((pixel >> 8 * 2) & 0xFF),
        .g = @intCast((pixel >> 8 * 1) & 0xFF),
        .b = @intCast((pixel >> 8 * 0) & 0xFF),
    };
}

pub fn jump(self: *Self) void {
    if (self.can_jump) {
        self.jump_steps = 5;
        self.can_jump = false;
    }
}

pub fn draw(self: Self, renderer: *sdl.SDL_Renderer) void {
    const rgb = pixel2rgb(self.color);
    _ = sdl.SDL_SetRenderDrawColor(renderer, rgb.r, rgb.g, rgb.b, 255);
    _ = sdl.SDL_RenderFillRect(renderer, @ptrCast(self.sdlrect));
}

pub fn updatePosition(self: *Self) void {
    var i: i32 = if (self.xvel >= 0) self.xvel else -self.xvel;

    while (i > 0) : (i -= 1) {
        self.sdlrect.x += @divExact(self.xvel, self.speed);
    }
}
